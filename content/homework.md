---
title: Homework - Git Training Workshop
date: 2022-02-04T21:48:46.000Z
draft: false
layout: single
lastmod: 2023-04-06T23:08:51.606Z
---

Congratulations on completing the TGDP Git Training Workshop! We have one last practice assignment for you to complete to help you solidify what you've learned today. Your task is to create your own **Git knowledge base** on GitLab. Even some of us who have been using Git for many years still need a cheat sheet and you'll definitely want to build a document of your own for this purpose.

This knowledge base should ultimately be whatever resource you need it to be. The content and organization is entirely up to you. But you could possibly use this knowledge base to:

* Record some of the common tasks you do in Git and the CLI commands you need to complete those tasks.
* Link to resources you find that have helped you on your Git learning journey.

You'll create this knowledge base as a new repository on your personal GitLab namespace. To create your knowledge base and contribute to it:

1. In GitLab on the top menu bar, click the **Create New** icon and select **New Project/repository**.
2. Click **Create blank project**.
3. Fill in the project details. 
   * **Project name**: Give your project a meaningful name like "My Git knowledge base."
   * **Project URL**: Click the menu and search for your GitLab username. Select the option that appears under Users and displays your username. 
   * **Project slug**: You can customize this URL if you want. Otherwise, leave it with the default setting suggested by GitLab. 
   * **Project deployment target**: Leave this optional setting as the default, which is to leave it blank. (It will say "Select the deployment target" if you don't select anything.)
   * **Visibility level**: Select the **Public** option so that you can show you completed your homework assignment to your Git workshop trainers and peers. 
   * **Project configuration**: Use the default settings. 
4. Click the **Create project** button. 
5. Since this repository is in your personal namespace, you won't need to fork it. Clone the repository to your local computer so that you can contribute to it. 
   {{< notice tip "I don't remember how to do these tasks in Git!" >}}If you get stuck doing any of the rest of these steps, try Googling the instructions to solve the problem on your own. If you get stuck, you can send a message to the Git trainers or post in our Slack channel, and we'll give you a hint.{{< /notice >}}
6. Create a new branch to add your initial content to your knowledge base. 
7. Open the project in VS Code and open the README.md file. You'll put the initial content in this README.md file. 
   {{< notice note "GitLab-flavored markdown includes an auto-generated TOC" >}}In files that will be displayed in GitLab, you don't need to create a table of contents (TOC) by hand. Insert this code after the title of your document to automatically generated a TOC:

   [TOC]
    
   For more info, see: [GitLab flavored markdown - Table of contents](https://docs.gitlab.com/ee/user/markdown.html#table-of-contents).{{< /notice>}}
8. Add the Git knowledge that you learned in today's workshop that you'd like to save for future reference. For example, you could include:
   * Helpful links and resources
   * How to fork and clone 
   * How to pull upstream changes into your local computer and push them up to your fork 
   * How to check out a new branch
   * How to stage and commit your changes, then push them upstream
9.  When you're done with your homework, let the Git trainers know by posting a follow-up message in our Slack channel with a link to your knowledge base!
