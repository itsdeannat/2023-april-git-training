## Description

The Good Dogs Project recently published a blog post about the top three criteria to consider when getting a new dog. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

There are three tasks you must complete. You'll complete your work in the `/content/blog/tips-for-your-first-dog.md` file.

### Fix a broken link

- [ ] Fix the broken link in the **Sources** section.

### Add a missing heading

- [ ] The **Lifestyle** section is missing a heading. Add a level two heading on line 15.

### There are only three tips, not four

- [ ] On line 2, change "4" to "Three" to reflect the number of tips in this blog post.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.