## Description

The Good Dogs Project recently published a blog post about why dogs eat grass. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

You need to complete three tasks. You'll complete your work in the `/content/blog/eating-grass.md` file.

### Fix link syntax

- [ ] Fix the broken link on line 28 under the **Keep boredom at bay** section.

### Add a bulleted list

- [ ] Convert the information in the **Other reasons why you dog may be eating grass** section to an unordered bulleted list. Use dashes (-) or asterisks (*) in front of each explanation.

### Change a level three heading to a level two heading

- [ ] Change the **Other reasons your dog may be eating grass** heading from a level three heading to a level two heading.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.