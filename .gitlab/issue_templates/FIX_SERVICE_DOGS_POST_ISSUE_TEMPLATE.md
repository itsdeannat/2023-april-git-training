## Description

The Good Dogs Project recently published a blog post about training your dog to be a service dog. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

You need to complete three tasks. You'll complete your work in the `/content/blog/service-dogs.md` file.

### Fix link syntax

- [ ] Fix the broken link in the **Best service dog breeds** section.

### Add a bulleted list

- [ ] Convert the information in the **What type of service dog will your dog be?** section to an unordered bulleted list. Use dashes (-) or asterisks (*) in front of each explanation.

### Change a level one heading to a level two heading

- [ ] Change the **Is my dog healthy?** heading from a level one heading to a level two heading.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.